﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hello : MonoBehaviour
{
    
    public GameObject crazySphere = new GameObject();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        Color randcolor = new Color(
            Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f)
        );


        Instantiate(crazySphere,new Vector3(0, 0, 0), Quaternion.identity);
        crazySphere.GetComponent<Renderer>().sharedMaterial.color = randcolor;
        
    }
}
